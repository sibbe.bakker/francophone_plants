# # Overview plots
# library(tidyverse)
# show <- c('FR', 'MG', 'ES', 'BR', 'US')
# 
# # First I plot the overview for which good wikidata is available...
# dt <- read_delim("../query_data/richness_plant_vs_number_people.tsv") %>% 
#   rename_with(stringr::str_replace, 
#               pattern = "\\?", replacement = "", 
#               matches("?"))
# 
# skimr::skim(dt)
# 
# richness_summary <- dt %>% 
#   select(alpha_2, name, year) %>% 
#   group_by(year, alpha_2) %>% 
#   summarise(n = n()) 
# 
# 
# pop_plant <- richness_summary %>% 
#   left_join(x = .,
#             y =  dt) %>% 
#   mutate(country_code = str_to_upper(country_code))
# 
# richness_plot_wk <- ggplot(pop_plant %>% 
#                           mutate(country = if_else(tolower(country_code) %in% tolower(show),
#                                                    true = country_code,
#                                                    false = 'other')),
#                         aes(y = n,
#                             x = year,
#                             fill = country))  +
#   geom_bar(stat = 'identity') +
#   labs(x = 'collection year',
#        y = 'amount of speciments collected') +
#   theme(legend.position = 'top') +
#   coord_cartesian(xlim = c(1760, 2010))
# richness_plot_wk
# ggsave('../analysis_output/richness_over_time_wk.pdf', richness_plot,
#        height = 5,
#        width = 10)
# 
# # Now for all the data
# dt <- read_delim("../query_data/richness_v_time.tsv") %>% 
#   rename_with(stringr::str_replace, 
#               pattern = "\\?", replacement = "", 
#               matches("?")) 
# skimr::skim(dt)
# 
# richness_summary <- dt %>% 
#   unique() %>% 
#   group_by(country_code, year) %>% 
#   summarise(n = n())
# 
# 
# 
# richness_plot <- ggplot(richness_summary %>% 
#                           mutate(country = if_else(tolower(country_code) %in% tolower(show),
#                                                    true = country_code,
#                                                    false = 'other')),
#                         aes(y = n,
#                             x = year,
#                             fill = country))  +
#   geom_bar(stat = 'identity') +
#   labs(x = 'collection year',
#        y = 'amount of speciments collected') +
#   theme(legend.position = 'top') +
#   coord_cartesian(xlim = c(1760, 2010))
# richness_plot
# ggsave('../analysis_output/richness_over_time_all.pdf', richness_plot,
#        height = 5,
#        width = 10)
