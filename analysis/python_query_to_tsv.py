#!/usr/bin/env python3
import sys
from SPARQLWrapper import SPARQLWrapper, CSV
sparql_file_path = sys.argv[1]
print(sys.argv)

with open(sparql_file_path, 'r') as f:
    lines_list = f.readlines()
query = ' '.join(lines_list)

sparql = SPARQLWrapper("http://localhost:7200/repositories/french_plants")
sparql.setQuery(query)
sparql.setReturnFormat(CSV)
results = sparql.query().convert()
print(results)
