from SPARQLWrapper import SPARQLWrapper2

sparql = SPARQLWrapper2("http://localhost:7200/repositories/french_plants")
sparql.setQuery("""
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select DISTINCT ?Genus ?species ?binNom ?label where { 
    ?observation rdf:type <http://example.com/plants/ObservationEvent>.
    ?observation <http://example.com/plants/observedPlant> ?plant.
   	?plant wdt:P225  ?name .
    BIND(REPLACE(?name, "\\(.+\\) .+", "") AS ?substring_no_paren)
    # ^((\w+\s*){0,2})(.*)$ gets the first two names.
    BIND(REPLACE(?name, "^((\\w+\\s*){0,2})(.*)$", "$1") AS ?first_two)
    
    BIND(REPLACE(?substring_no_paren, "(var\\.) .*", "") AS ?substring_2)
    BIND(REPLACE(?substring_2, "(subsp\\.) .*", "") AS ?substring_3)
    BIND(REPLACE(?substring_3, "\\w+\\.", "") AS ?substring_4)
    
    # Subspecies retrieval
    BIND(REPLACE(?substring_no_paren, ".+ subsp.", "") AS ?subspecies) 
    BIND(REPLACE(?subspecies, "\\s/^\\s+|\\s+$|\\s+(?=\\s)//g", "") AS ?subspecies_complete)
   
    # REGEX from https://stackoverflow.com/a/19020103.
    BIND(REPLACE(?first_two, "\\s/^\\s+|\\s+$|\\s+(?=\\s)//g", "") AS ?first_two_no_spaces)
   
    BIND(strbefore(?first_two_no_spaces," ") as ?genus) 
    BIND(strafter(?first_two_no_spaces," ") as ?species) 
    BIND(CONCAT(UCASE(SUBSTR(?genus, 1, 1)), SUBSTR(?genus, 2)) as ?Genus)
    BIND(CONCAT(?Genus, " ", ?species) as ?binNom)
    
    SERVICE <https://query.wikidata.org/sparql> {
        ?item wdt:P225 ?binNom.
        ?item wdt:P141 ?status.
  		?status  rdfs:label ?label.
  		FILTER (lang(?label) = 'en')
    }
               } LIMIT 1:00


"""
)

try:
    ret = sparql.query()
    print(ret.variables)  # this is an array consisting of "subj" and "prop"
    for binding in ret.bindings:
        # each binding is a dictionary. Let us just print the results
        print(f"{binding['subj'].value}, {binding['subj'].type}")
        print(f"{binding['prop'].value}, {binding['prop'].type}")
except Exception as e:
    print(e)
