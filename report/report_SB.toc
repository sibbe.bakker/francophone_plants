\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Methods}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Data gathering}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Data refinement}{3}{subsection.2.2}%
\contentsline {paragraph}{Data cleanup}{3}{section*.2}%
\contentsline {paragraph}{Ontology creation}{3}{section*.3}%
\contentsline {paragraph}{Transformation to graph data}{4}{section*.5}%
\contentsline {subsection}{\numberline {2.3}Data analysis and queries}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Results}{5}{section.3}%
\contentsline {paragraph}{Species richness over time}{5}{section*.6}%
\contentsline {paragraph}{Relation between country population and sampling}{6}{section*.8}%
\contentsline {paragraph}{Conservation status}{8}{section*.10}%
\contentsline {section}{\numberline {4}Discussion}{10}{section.4}%
\contentsline {paragraph}{Main findings}{10}{section*.12}%
\contentsline {paragraph}{Limitations of this work}{10}{section*.13}%
\contentsline {paragraph}{Suggestions for future work}{10}{section*.14}%
\contentsline {paragraph}{Concluding remarks}{11}{section*.15}%
\contentsline {section}{Appendices}{11}{section*.16}%
\contentsline {subsection}{\numberline {A}Code availability}{11}{subsection.Alph0.1}%
\contentsline {subsection}{\numberline {B}\texttt {SPARQL} queries}{12}{subsection.Alph0.2}%
\contentsline {subsubsection}{\numberline {B-1}Country data}{12}{subsubsection.Alph0.2.1}%
\contentsline {subsubsection}{\numberline {B-2}Conservation status}{13}{subsubsection.Alph0.2.2}%
