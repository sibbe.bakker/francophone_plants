# `francophone_plants` README file

- Sibbe Bakker 2022-XII-20

![badge](https://img.shields.io/badge/version-final-brightgreen 'final version')

This is the read me file for the linked data github repository. In this repository, the analysis source code, data, UML diagramme and the report with its references can be found.

## File manifest tree

```
.
├── analysis
│   ├── analysis
│   │   ├── analysis.Rproj
│   │   ├── extincions.R
│   │   ├── local_query.R
│   │   ├── over_view.R
│   │   ├── sampling_ana_population.R
│   │   └── species_v_time.R
│   ├── analysis_output
│   │   ├── conservation_overview_europe.pdf
│   │   ├── conservation_overview_map.pdf
│   │   ├── conservation_overview.pdf
│   │   ├── population.pdf
│   │   ├── richness_over_time_all.pdf
│   │   ├── richness_over_time.pdf
│   │   ├── richness_over_time_wk.pdf
│   │   └── richness_v_population.pdf
│   ├── python_query_to_tsv.py
│   ├── query_data
│   │   ├── conservation_status_no_comment.sparql
│   │   ├── conservation_status.sparql
│   │   ├── conservation_status.tsv
│   │   ├── country_overview_full.sparql
│   │   ├── plant_data.sparql
│   │   ├── random_select.sparql
│   │   ├── richness_plant_vs_number_people.sparql
│   │   ├── richness_plant_vs_number_people.tsv
│   │   ├── richness_v_time.sparql
│   │   └── richness_v_time.tsv
│   ├── rich.csv
│   └── test.py
├── data
│   ├── data_summary.txt
│   ├── input_data
│   │   ├── country_codes.csv
│   │   ├── occurrence.txt
│   │   ├── plants.csv
│   │   └── sample.txt
│   ├── linked_data.Rproj
│   ├── output_data
│   │   ├── plants.csv
│   │   ├── plants.ttl
│   │   ├── small_plants.csv
│   │   └── small_plants.ttl
│   ├── prelim_work.R
│   └── transformation_data
│       └── mapping.json
├── diagramme
│   └── plant_observations
├── README.md
├── referenced_works
│   ├── files
│   │   ├── 14876
│   │   │   └── Le Bras et al. - 2017 - The French Muséum national d’histoire naturelle va.pdf
│   │   ├── 14877
│   │   │   └── sdata201716.html
│   │   ├── 14883
│   │   │   └── Köberl et al. - 2013 - The microbiome of medicinal plants diversity and .pdf
│   │   ├── 14885
│   │   │   └── S1433831914000316.html
│   │   ├── 14886
│   │   │   └── brv.html
│   │   ├── 14887
│   │   │   └── Soliveres and Maestre - 2014 - Plant–plant interactions, environmental gradients .pdf
│   │   ├── 14889
│   │   │   └── Chen et al. - 2019 - Meta-analysis shows positive effects of plant dive.pdf
│   │   ├── 14891
│   │   │   └── s41467-019-09258-y.html
│   │   ├── 14892
│   │   │   └── Wagner et al. - 2021 - Insect decline in the Anthropocene Death by a tho.pdf
│   │   ├── 14894
│   │   │   └── Christenhusz and Byng - 2016 - The number of known plants species in t.pdf
│   │   ├── 14896
│   │   │   └── Pimm and Joppa - 2015 - How Many Plant Species are There, Where are They, .pdf
│   │   ├── 14899
│   │   │   └── Laban et al. - 2018 - Soil biodiversity and soil organic carbon keeping.pdf
│   │   ├── 14900
│   │   │   └── j.1365-2389.2010.01314.html
│   │   ├── 14903
│   │   │   └── Bardgett and van der Putten - 2014 - Belowground biodiversity and ecosystem functioning.pdf
│   │   ├── 14904
│   │   │   └── nature13855.html
│   │   ├── 14923
│   │   │   └── graphdb.ontotext.com.html
│   │   ├── 14925
│   │   │   └── sf.html
│   │   ├── 14927
│   │   │   └── sparql.html
│   │   ├── 14945
│   │   │   └── Darrah et al. - 2017 - Using coarse-scale species distribution data to pr.pdf
│   │   ├── 14946
│   │   │   └── ddi.html
│   │   ├── 14949
│   │   │   └── Humphreys et al. - 2019 - Global dataset shows geography and life form predi.pdf
│   │   ├── 14950
│   │   │   └── Vamosi et al. - 2018 - Macroevolutionary Patterns of Flowering Plant Spec.pdf
│   │   ├── 14953
│   │   │   └── Corlett - 2016 - Plant diversity in a changing world Status, trend.pdf
│   │   ├── 14954
│   │   │   └── S2468265916300300.html
│   │   ├── 14956
│   │   │   └── Chen et al. - 2018 - Plant diversity enhances productivity and soil car.pdf
│   │   ├── 14960
│   │   │   └── Wang et al. - 2019 - Soil microbiome mediates positive plant diversity-.pdf
│   │   ├── 14962
│   │   │   └── S1439179110000800.html
│   │   ├── 14963
│   │   │   └── j.1461-0248.2010.01548.html
│   │   ├── 14964
│   │   │   └── ele.html
│   │   ├── 14967
│   │   │   └── Quijas et al. - 2010 - Plant diversity enhances provision of ecosystem se.pdf
│   │   ├── 14969
│   │   │   └── Borgelt et al. - 2022 - More than half of data deficient species predicted.pdf
│   │   ├── 14971
│   │   │   └── Vaglica et al. - 2017 - Monitoring internet trade to inform species conser.pdf
│   │   ├── 14973
│   │   │   └── Pelletier et al. - 2018 - Predicting plant conservation priorities on a glob.pdf
│   │   ├── 14975
│   │   │   └── Gillson et al. - 2020 - What Are the Grand Challenges for Plant Conservati.pdf
│   │   ├── 14977
│   │   │   └── James et al. - 2018 - Herbarium data Global biodiversity and societal b.pdf
│   │   ├── 14978
│   │   │   └── aps3.html
│   │   ├── 14980
│   │   │   └── Jones and Daehler - 2018 - Herbarium specimens can reveal impacts of climate .pdf
│   │   ├── 14982
│   │   │   └── Nic Lughadha et al. - 2019 - The use and misuse of herbarium specimens in evalu.pdf
│   │   ├── 14984
│   │   │   └── Yang et al. - 2022 - PlantNet transfer learning-based fine-grained net.pdf
│   │   ├── 14987
│   │   │   └── Wolf et al. - 2022 - Citizen science plant observations encode global t.pdf
│   │   ├── 14988
│   │   │   └── 1365-2664.html
│   │   ├── 14989
│   │   │   └── Khapugin et al. - 2021 - ADDITIONS TO THE VASCULAR PLANT FLORA OF THE REPUB.pdf
│   │   ├── 14996
│   │   │   └── Seregin et al. - 2020 - Flora of Russia on iNaturalist a dataset.pdf
│   │   ├── 14997
│   │   │   └── Young et al. - 2021 - Using Citizen Science Observations to Develop Mana.pdf
│   │   ├── 14999
│   │   │   └── Van Horn et al. - 2018 - The INaturalist Species Classification and Detecti.pdf
│   │   ├── 15001
│   │   │   └── Boakes et al. - 2010 - Distorted Views of Biodiversity Spatial and Tempo.pdf
│   │   ├── 15003
│   │   │   └── Mahecha et al. - 2021 - Crowd-sourced plant occurrence data provide a reli.pdf
│   │   ├── 15005
│   │   │   └── ecog.html
│   │   ├── 15006
│   │   │   └── j.1354-1013.2001.00467.html
│   │   ├── 15008
│   │   │   └── Craine et al. - 2011 - Functional consequences of climate change-induced .pdf
│   │   ├── 15011
│   │   │   └── Bakkenes et al. - 2002 - Assessing effects of forecasted climate change on .pdf
│   │   ├── 15013
│   │   │   └── Willis - 2019 - Rarefaction, Alpha Diversity, and Statistics.pdf
│   │   ├── 15016
│   │   │   └── Thiers et al. - 2016 - Digitization of The New York Botanical Garden Herb.pdf
│   │   └── 15018
│   │       └── 50c9509d-22c7-4a22-a47d-8c48425ef4a7.html
│   └── referenced_works.bib
└── report
    ├── figures
    │   ├── abundance_whole_data_set.png
    │   ├── diagramme_SB.pdf
    │   └── observations_per_year.pdf
    ├── report_SB.aux
    ├── report_SB.bbl
    ├── report_SB.bcf
    ├── report_SB.blg
    ├── report_SB.fdb_latexmk
    ├── report_SB.fls
    ├── report_SB.log
    ├── report_SB.out
    ├── report_SB.pdf
    ├── report_SB.run.xml
    ├── report_SB.synctex.gz
    ├── report_SB.tex
    └── report_SB.toc

67 directories, 112 files
```




